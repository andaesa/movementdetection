import argparse, datetime, imutils, time, cv2

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
#ap.add_argument("-v","--video", help = "path to the video file") ## use this to read from a video file ##
ap.add_argument("-a","--min-area", type = int, default = 500, help = "minimum area size")
args = vars(ap.parse_args())

#reading from webcam
camera = cv2.VideoCapture(0)

firstFrame = None # stores the first frameof the video file/webcam stream

# loop over the frames of the video
while True:
	(grabbed, frame) = camera.read()
	text = "unoccupied"

	if not grabbed:
		break
    
    # resize the frame, convert it into grayscale and blur it
	frame = imutils.resize(frame, width = 500)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (21,21),0)

	if firstFrame is None:
		firstFrame = gray
		continue

    # compute the absolute difference between the current frame and first frame 
	frameDelta = cv2.absdiff(firstFrame, gray)
	thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

    # dilate the threshold image to fill in holes, then find contours on threshold image
	thresh = cv2.dilate(thresh, None, iterations = 2)
	(cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    # loop over the contours, if the contours is too small, ignore it
	for c in cnts:
		if cv2.contourArea(c) < args["min_area"]:
			continue

        # compute the bounding box for the contour, draw it on the frame and update the text
		(x, y, w, h) = cv2.boundingRect(c)
		cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
		text = "occupied"
    
    # draw the text and timeframe on the frame
	cv2.putText(frame, "Room Status: {}".format(text), (10,20),
		cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
	cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
		(10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
    
    # show the frame
	cv2.imshow("webcam", frame)
	key = cv2.waitKey(10) & 0xFF

    # close the window if 'escape' key is pressed
	if key == 27:
		break

camera.release()
cv2.destroyAllWindows()

# # # reference : http://www.pyimagesearch.com/2015/05/25/basic-motion-detection-and-tracking-with-python-and-opencv/ 

