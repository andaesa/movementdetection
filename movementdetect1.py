#using differential image
import cv2

def diffImg(t0,t1,t2):
	d1 = cv2.absdiff(t2,t1)
	d2 = cv2.absdiff(t1,t0)
	return cv2.bitwise_and(d1, d2)

# read from camera
camera = cv2.VideoCapture(0)

winName = "Movement Detector"
cv2.namedWindow(winName, cv2.CV_WINDOW_AUTOSIZE)

# read first 3 image
t_minus = cv2.cvtColor(camera.read()[1], cv2.COLOR_BGR2GRAY)
t = cv2.cvtColor(camera.read()[1], cv2.COLOR_BGR2GRAY)
t_plus = cv2.cvtColor(camera.read()[1], cv2.COLOR_BGR2GRAY)

# loop over the images
while True:
	cv2.imshow(winName, diffImg(t_minus, t, t_plus))

    # read the next image
	t_minus = t
	t = t_plus
	t_plus = cv2.cvtColor(camera.read()[1], cv2.COLOR_BGR2GRAY)
    
    # close window when 'escape' key is pressed
	key = cv2.waitKey(10)
	if key == 27:
		cv2.destroyAllWindows()
		break


# # # reference : http://www.steinm.com/blog/motion-detection-webcam-python-opencv-differential-images/